create table AUTHOR
(
    ID      BIGINT       not null
        primary key,
    NAME    VARCHAR(255) not null,
    SURNAME VARCHAR(255) not null
);

create table FIRM
(
    ID                       BIGINT       not null
        primary key,
    DEFAULT_MAX_BORROW_COUNT INTEGER,
    DEFAULT_MAX_LENT_DAYS    INTEGER,
    NAME                     VARCHAR(255) not null,
    TYPE                     INTEGER      not null
);

create table LANGUAGE
(
    ID   BIGINT       not null
        primary key,
    NAME VARCHAR(255) not null
);

create table BOOK
(
    ID        BIGINT       not null
        primary key,
    ISBN      VARCHAR(255) not null
        unique,
    PUBLISHED DATE         not null,
    TITLE     VARCHAR(255) not null,
    LANG_ID   BIGINT,
    constraint FKCQCFY3DK0T47IEFHBX0M6SVIL
        foreign key (LANG_ID) references LANGUAGE
);

create table BOOK_AUTHORS
(
    BOOK_ID    BIGINT not null,
    AUTHORS_ID BIGINT not null,
    constraint FK551I3SLLW1WJ7EX6NIR16BLSM
        foreign key (AUTHORS_ID) references AUTHOR,
    constraint FKS4XM7Q8I3UXVAISWJ1C35NNXW
        foreign key (BOOK_ID) references BOOK
);

create table LIBRARY
(
    ID   BIGINT not null
        primary key,
    NAME VARCHAR(255)
);

create table LIBRARY_ENTRY
(
    ID      BIGINT not null
        primary key,
    AMOUNT  INTEGER,
    NAME    VARCHAR(255),
    BOOK_ID BIGINT,
    constraint FK48TAX1YEE4FOWYW4WNSN7OYQF
        foreign key (BOOK_ID) references BOOK
);

create table LIBRARY_GROUP
(
    ID   BIGINT not null
        primary key,
    NAME VARCHAR(255)
);

create table LIBRARY_GROUP_LIBRARIES
(
    LIBRARY_GROUP_ID BIGINT not null,
    LIBRARIES_ID     BIGINT not null
        unique,
    constraint FKFLBYSPFNXTW8FEE45WCALY4RV
        foreign key (LIBRARIES_ID) references LIBRARY,
    constraint FKGNNYK5I93R9LBPMCP9HBYCEGQ
        foreign key (LIBRARY_GROUP_ID) references LIBRARY_GROUP
);

create table LIBRARY_LIBRARY_ENTRIES
(
    LIBRARY_ID         BIGINT not null,
    LIBRARY_ENTRIES_ID BIGINT not null
        unique,
    constraint FKCJ7964I2OOHUPNB6H40WSC1T6
        foreign key (LIBRARY_ID) references LIBRARY,
    constraint FKCQ88GLS4R5W5HK2S5886F80J7
        foreign key (LIBRARY_ENTRIES_ID) references LIBRARY_ENTRY
);

create table USER
(
    ID         BIGINT       not null
        primary key,
    EMAIL      VARCHAR(255) not null
        unique,
    NAME       VARCHAR(255) not null,
    STATUS     INTEGER      not null,
    SURNAME    VARCHAR(255) not null,
    FIRM_ID    BIGINT,
    LIBRARY_ID BIGINT,
    constraint FKBFBRR0DI61IXDF940WFBD9V6R
        foreign key (LIBRARY_ID) references LIBRARY,
    constraint FKLAWVBHUSEVJFGJRCGBF15SISQ
        foreign key (FIRM_ID) references FIRM
);

create table BORROW
(
    ID                   BIGINT    not null
        primary key,
    BORROW_DATE          TIMESTAMP not null,
    EXPECTED_RETURN_DATE TIMESTAMP not null,
    LIBRARY_ENTRY_ID     BIGINT,
    USER_ID              BIGINT,
    constraint FKFH1XYIYE9CEWCR7RWCY90PUIT
        foreign key (LIBRARY_ENTRY_ID) references LIBRARY_ENTRY,
    constraint FKTLX8CBAFJLYP2HGFOG0BDMNI3
        foreign key (USER_ID) references USER
);

