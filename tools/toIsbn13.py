from isbnlib import to_isbn13

with open("isbn10.txt", 'r') as fr:
    with open("isbn13.txt", 'w') as fw:
        lines = fr.readlines()
        for isbn10 in lines:
            isbn13 = to_isbn13(isbn10)
            fw.write(isbn13 + "\n")
