# Projekt “Firmowa biblioteka”


### A. Cel

Celem projektu jest stworzenie usług na potrzeby systemu
zarządzającego firmową biblioteką liczącą sobie tysiące pozycji
podzielonych na kategorie tematyczne (np. Programowanie, Bazy
danych, Zarządzanie itp.). Firma posiada książki, które mogą być
wypożyczane przez pracowników na umówiony okres czasu. Po
upłynięciu okresu wypożyczenia pracownik zobowiązany jest zwrócić
książkę.
Dodatkowo każdy pracownik może w danym momencie posiadać
jedynie ograniczoną liczbę wypożyczonych książek tak by dać szansę
innym pracownikom na przeczytanie tytułu, który u niego czekałby
na swój czas i zarastał kurzem.
Przewidziane są wdrożenia systemu bibliotecznego w
zaprzyjaźnionych firmach tak by pracownicy mieli do dyspozycji
większą liczbę książek. Wiąże się to z koniecznością zrealizowania
integracji systemu bibliotecznego firmy A z instancją uruchomioną
w firmie partnerskiej B – w obydwu firmach funkcjonować będzie
dokładnie taka sama wersja systemu, lecz zarządzająca niezależnymi
zbiorami literatury. Zatem system niezależnie od firmy, w której
został wdrożony, może zawierać użytkowników będących pracownikami
firmy rodzimej lub zaprzyjaźnionej.

### B. Wymagania funkcjonalne
1. Zarządzanie użytkownikami:
- Dodawanie nowych użytkowników z podstawowymi
informacjami:Imię,Nazwisko,Adres e-mail (wartość unikalna),Identyfikator firmy
- Pobierania danych o użytkowniku poprzez wskazanie
unikalnego identyfikatora lub adresu e-mail nadanego
przez system. Oczekiwany zakres danych: Identyfikator systemowy, Imię, Nazwisko, Adres e-mail, Status (aktywny/zablokowany)
- Blokowanie użytkownika tak by nie mógł korzystać ze
zbiorów bibliotecznych
- Odblokowanie użytkownika tak by mógł korzystać ze
zbiorów bibliotecznych

2. Zarządzanie autorami
- Dodawania autorów z podstawowymi danymi osobowymi: Imię, Nazwisko
- Pobieranie danych o autorze poprzez wskazanie unikalnego
identyfikatora nadanego przez system. Oczekiwany zakres
danych: Identyfikator systemowy, Imię, Nazwisko
3. Zarządzanie zbiorem biblioteki
- Dodawanie książek wraz z wymaganymi danymi: Tytułem, Językiem, Numerem ISBN (wartość unikalna), Datą publikacji, Identyfikatorem systemowym autora
- Pobieranie książek spełniających zadane kryteria
wyszukiwania (filtrowanie po: tytule, numerze ISBN,
dacie publikacji, języku, identyfikatorze autora) wraz
z możliwością stronicowania wyników wyszukiwania –
klient API może, ale nie musi korzystać z opcji
stronicowania. Oczekiwany zakres danych dla
pojedynczego elementu: Identyfikator systemowy książki, Tytułem, Językiem, Numerem ISBN, Datą publikacji, Identyfikatorem systemowym autora

4. Wypożyczenia
- Wypożyczenia książki wskazanej poprzez przekazanie do
systemu numeru ISBN oraz informacji, czy użytkownik
akceptuje książki znajdujące w zbiorach zaprzyjaźnionej
firmy
- System powinien sprawdzać czy użytkownik nie
został zablokowany oraz czy nie przekroczył limitu
liczby wypożyczeń
- Jeżeli książka nie występuje w zbiorze jego firmy
lub jest zajęta, a użytkownik akceptuje książki
pochodzące ze zbiorów firmy zaprzyjaźnionej,
wówczas system powinien dokonać wypożyczenia
książki z systemu firmy zaprzyjaźnionej (o ile
znajduje się ona w jej zbiorze)
- By system mógł dokonać wypożyczenia (w imieniu
użytkownika) książki w bibliotece firmy
partnerskiej, użytkownik musi posiadać konto tejże bibliotece. Zakłada się, że jeżeli w obu
bibliotekach występują użytkownicy o takim samym
adresie e-mail, wówczas traktuje ich jako tego
samego pracownika jednej z firm.

### B. Uruchomienie:
pl.o4b.LibraryApplication

### B. Dokumentacja Api:
Swagger/openapi ui dostępny po uruchomieniu aplikacji pod adresem  http://localhost:8081/library/swagger-ui.html

