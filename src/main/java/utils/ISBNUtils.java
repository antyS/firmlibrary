package utils;

import java.util.Random;

public class ISBNUtils {
    private static final Random RANDOM = new Random(1);


    public static String normalizeISBN(String isbn) {
        return isbn.replaceAll("-", "");
    }

    public static String generateISBN() {
        StringBuilder isbn = new StringBuilder();

        for (int i = 0; i < 13; i++) {
            isbn.append(RANDOM.nextLong() % 10);
        }
        return isbn.toString();
    }
}
