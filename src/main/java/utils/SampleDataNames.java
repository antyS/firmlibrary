package utils;

public class SampleDataNames {
    public static final String BOOK = "book";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String LIBRARY = "library";
    public static final String GROUP = "group";
    public static final String ENTRY = "entry";
    public static final String EMAIL_PREFIX = "example";
    public static final String EMAIL_SUFFIX = "@example.com";
}

