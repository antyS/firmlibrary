package utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class IdGenerator {

    private static final Random RANDOM = new Random(1);

    public static Long generateRandomID(int offset){
        return Math.abs(RANDOM.nextLong())%100 + offset;
    }

    public static List<Long> generateRandomIDList(int offset){
        List<Long> idList = new ArrayList<>();
        while(idList.size()<5){
            Long id = generateRandomID(offset);
            if(!idList.contains(id)){
                idList.add(id);
            }
        }
        return idList;
    }

}
