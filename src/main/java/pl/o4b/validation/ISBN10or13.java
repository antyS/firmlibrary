package pl.o4b.validation;

import org.hibernate.validator.constraints.ConstraintComposition;
import org.hibernate.validator.constraints.ISBN;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.hibernate.validator.constraints.CompositionType.OR;

@ISBN(type = ISBN.Type.ISBN_10)
@ConstraintComposition(OR)
@ISBN(type = ISBN.Type.ISBN_13)
@ReportAsSingleViolation
@Target({METHOD, FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = {})
public @interface ISBN10or13 {
    String message() default "{pl.o4b.validation.NullOrPositiveOrZero.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
