package pl.o4b.config;

import lombok.extern.slf4j.Slf4j;
import org.reflections.ReflectionUtils;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import pl.o4b.web.dto.response.SystemResponse;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class FilterAdvice implements ResponseBodyAdvice<Object> {
    private static final String INCLUDE_KEY = "include";
    private static final String EXCLUDE_KEY = "exclude";

    @Override
    public boolean supports(final MethodParameter returnType, final Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(final Object body, final MethodParameter returnType, final MediaType selectedContentType,
                                  final Class<? extends HttpMessageConverter<?>> selectedConverterType, final ServerHttpRequest request,
                                  final ServerHttpResponse response) {
        HttpServletRequest httpServletRequest = ((ServletServerHttpRequest) request).getServletRequest();
        if (canFilterResponse(body, httpServletRequest)) {
            List<String> includeFields = getParameterAsList(httpServletRequest, INCLUDE_KEY);
            List<String> excludeFields = getParameterAsList(httpServletRequest, EXCLUDE_KEY);
            final Set<Field> allFields = ReflectionUtils.getFields(body.getClass());
            List<Field> fieldsToWipe = allFields.stream().filter(isExcludedOrNotIncluded(excludeFields, includeFields))
                    .collect(Collectors.toList());
            fieldsToWipe.forEach(field -> {
                try {
                    field.set(body, null);
                } catch (IllegalAccessException | SecurityException e) {
                    log.error("Error while trying to filter response fields");
                }
            });
        }
        return body;
    }

    private boolean canFilterResponse(final Object body, final HttpServletRequest httpServletRequest) {
        return (body instanceof SystemResponse && (httpServletRequest.getParameterMap().containsKey(INCLUDE_KEY)
                || httpServletRequest.getParameterMap().containsKey(EXCLUDE_KEY))
        );
    }

    private List<String> getParameterAsList(final HttpServletRequest request, final String listParameterName) {
        List<String> result = new ArrayList<>();
        if (request.getParameterMap().containsKey(listParameterName)) {
            result = Arrays.asList(request.getParameter(listParameterName).split(","));
        }
        return result;
    }

    private Predicate<Field> isExcludedOrNotIncluded(final List<String> excludeFields, final List<String> includeFields) {
        return field -> excludeFields.contains(field.getName()) || !includeFields.contains(field.getName());
    }
}