package pl.o4b.web.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;
import pl.o4b.service.BookService;
import pl.o4b.web.dto.request.CreateBookRequest;
import pl.o4b.web.dto.response.BookResponse;
import pl.o4b.web.dto.response.UserResponse;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static pl.o4b.web.controller.paths.Paths.BOOKS;

@RestController
@RequestMapping(path = BOOKS)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
class BookController {

    private final BookService bookService;

    @PostMapping
    @Operation(summary = "Creates new BOOK, returns newly created book id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Newly created book id",
                    content = {@Content(mediaType = MimeTypeUtils.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Long.class))}),})
    public ResponseEntity<?> createBook(@RequestBody @Valid final CreateBookRequest bookRequest) {
        Long bookId = bookService.createBook(bookRequest);
        return ResponseEntity.status(CREATED).body(bookId);
    }

    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    @Operation(summary = "Returns information about specific book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found book",
                    content = {@Content(mediaType = MimeTypeUtils.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = BookResponse.class))}),
            @ApiResponse(responseCode = "404", description = "Book not found",
                    content = @Content)})
    public ResponseEntity<?> findBook(@PathVariable final Long id){
        BookResponse bookResponse = bookService.findBook(id);
        return ResponseEntity.ok(bookResponse);
    }

    @Operation(summary = "Delete selected book")
    @DeleteMapping(path = "/{id}")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Book deleted successfully",
                    content = {@Content(mediaType = MimeTypeUtils.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation =  Long.class))}),
    })
    public ResponseEntity<?> deleteBook(@PathVariable final Long id){
        bookService.deleteBook(id);
        return ResponseEntity.status(ACCEPTED).build();
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @Operation(summary = "Returns information about specific book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found book",
                    content = {@Content(mediaType = MimeTypeUtils.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = BookResponse.class))}),
            @ApiResponse(responseCode = "404", description = "Book not found",
                    content = @Content)})
    public @ResponseBody List<BookResponse> getAllBooks(){
        return bookService.getAll();
    }

}
