package pl.o4b.web.controller.RepositoryRestResource;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.o4b.db.model.book.Author;

import java.util.List;

import static pl.o4b.web.controller.paths.Paths.AUTHORS;

@RepositoryRestResource(collectionResourceRel = AUTHORS, path = AUTHORS)
public interface AuthorCrudRepository extends PagingAndSortingRepository<Author, Long> {

    List<Author> findByName(@Param("name") String name);

    List<Author> findBySurname(@Param("surname") String surname);

    List<Author> findByNameAndSurname(@Param("name") String name, @Param("surname") String surname);
}