package pl.o4b.web.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.db.model.user.Status;
import pl.o4b.exception.NoSuchLibraryEntryExeption;
import pl.o4b.exception.UserBlockedException;
import pl.o4b.service.*;
import pl.o4b.web.dto.request.CreateBorrowRequest;
import pl.o4b.web.dto.request.CreateUserRequest;
import pl.o4b.web.dto.response.LibraryEntryResponse;
import pl.o4b.web.dto.response.LibraryGroupResponse;
import pl.o4b.web.dto.response.LibraryResponse;
import pl.o4b.web.dto.response.UserResponse;

import javax.validation.Valid;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static pl.o4b.web.controller.paths.Paths.*;

@Slf4j
@RestController
@RequestMapping(BORROW)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class BorrowController {

    UserService userService;
    BorrowService borrowService;
    LibraryGroupService libraryGroupService;
    LibraryService libraryService;
    LibraryEntryService libraryEntryService;

    @Operation(summary = "Borrow book for user from any library.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "book borrowed successfully",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation =  Long.class))}),
    })
    @PutMapping(path = BORROW_FROM_ALL + "/{userId}/{bookId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> borrowBookFromAll(@PathVariable final Long userId, @PathVariable final Long bookId) {
        UserResponse user = userService.findUser(userId);
        if(user.status == Status.BLOCKED){
            throw new UserBlockedException();
        }
        Boolean bookFound = false;
        Long currentId = Long.valueOf(0);
        List<LibraryResponse> libraries = libraryService.getAll();
        for(LibraryResponse library : libraries){
            for (Long entryId : library.libraryEntryIds) {
                bookFound = libraryEntryService.borrowBook(entryId, bookId);
                currentId = entryId;
            }
            if(bookFound){
                break;
            }
        }
        borrowService.createBorrow(new CreateBorrowRequest(userId,currentId, new Date(), new Date()));
        return ResponseEntity.status(ACCEPTED).build();
    }

    @Operation(summary = "Borrow book for user.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "book borrowed successfully",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation =  Long.class))}),
    })
    @PutMapping(path = BORROW + "/{userId}/{bookId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> borrowBook(@PathVariable final Long userId, @PathVariable final Long bookId) {
        UserResponse user = userService.findUser(userId);
        if(user.status == Status.BLOCKED){
            throw new UserBlockedException();
        }
        Long currentId = Long.valueOf(0);
        LibraryResponse library = libraryService.findLibrary(user.libraryId);
        for( Long entryId : library.libraryEntryIds){
            libraryEntryService.borrowBook(entryId, bookId);
            currentId = entryId;
        }
        Date future = Date.from(LocalDate.now().plusMonths(3).atStartOfDay(ZoneId.systemDefault()).toInstant());
        borrowService.createBorrow(new CreateBorrowRequest(userId,currentId, new Date(), future));
        return ResponseEntity.status(ACCEPTED).build();
    }


    @Operation(summary = "Return book by user.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "book returned successfully",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation =  Long.class))}),
    })
    @PutMapping(path = RETURN + "/{userId}/{bookId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> returnBook(@PathVariable final Long userId, @PathVariable final Long bookId) {
        UserResponse user = userService.findUser(userId);
        if(user.status == Status.BLOCKED){
            throw new UserBlockedException();
        }
        LibraryResponse library = libraryService.findLibrary(user.libraryId);
        for( Long entryId : library.libraryEntryIds){
            libraryEntryService.returnBook(entryId, bookId);
        }
        borrowService.deleteBorrow(userId);
        return ResponseEntity.status(ACCEPTED).build();
    }
    
}
