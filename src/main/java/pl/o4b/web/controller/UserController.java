package pl.o4b.web.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.o4b.service.UserService;
import pl.o4b.web.dto.request.CreateUserRequest;
import pl.o4b.web.dto.response.UserResponse;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static pl.o4b.web.controller.paths.Paths.*;

@Slf4j
@RestController
@RequestMapping(USERS)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class UserController {

    UserService userService;

    @Operation(summary = "Returns information about specific user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found user",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = UserResponse.class))}),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content)})
    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findUser(@PathVariable final Long id) {
        UserResponse user = userService.findUser(id);
        return ResponseEntity.ok(user);
    }

    @PostMapping
    @Operation(summary = "Creates new user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "newly created user id",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Long.class))}),})
    public ResponseEntity<?> createUser(@RequestBody @Valid final CreateUserRequest createUserRequest) {
        Long userId = userService.createUser(createUserRequest);
        return ResponseEntity.status(CREATED).body(userId);
    }

    @PutMapping(path = "/{id}/" + BLOCK)
    @Operation(summary = "Blocks user by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "blocked successful",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Long.class))}),})
    public ResponseEntity<?> blockUser(@PathVariable final Long id) {
        userService.blockUser(id);
        return ResponseEntity.status(NO_CONTENT).build();
    }

    @PutMapping(path = "/{id}/" + UNBLOCK)
    @Operation(summary = "Unblocks user by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "unblocked successful",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Long.class))}),})
    public ResponseEntity<?> unblockUser(@PathVariable final Long id) {
        userService.unblockUser(id);
        return ResponseEntity.status(NO_CONTENT).build();
    }

    @Operation(summary = "Delete selected user")
    @DeleteMapping(path = "/{id}")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "user deleted successfully",
                content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation =  Long.class))}),
    })
    public ResponseEntity<?> deleteUser(@PathVariable final Long id){
        userService.deleteUser(id);
        return ResponseEntity.status(ACCEPTED).build();
    }

    @PutMapping
    @Operation(summary = "Updates ot creates new user if selected ID is not found")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated user id",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Long.class))}),})
    public ResponseEntity<?> createOrUpdateUser(@RequestBody @Valid final CreateUserRequest createUserRequest) {
        Long userId = userService.createOrUpdateUser(createUserRequest);
        return ResponseEntity.status(CREATED).body(userId);
    }

}
