package pl.o4b.web.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.service.AuthorService;
import pl.o4b.service.LibraryEntryService;
import pl.o4b.web.dto.request.CreateAuthorRequest;
import pl.o4b.web.dto.request.CreateLibraryEntryRequest;
import pl.o4b.web.dto.response.AuthorResponse;
import pl.o4b.web.dto.response.LibraryEntryResponse;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static pl.o4b.web.controller.paths.Paths.AUTHORS;
import static pl.o4b.web.controller.paths.Paths.LIBRARY_ENTRIES;

@RestController
@RequestMapping(LIBRARY_ENTRIES)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
class LibraryEntryController {
    private final LibraryEntryService libraryEntryService;

    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    @Operation(summary = "Returns info about libraryEntry with given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Library entry found",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = LibraryEntry.class))}),
            @ApiResponse(responseCode = "404", description = "Library entry not found",
                    content = @Content)
    })
    public ResponseEntity<?> findLibraryEntry(@PathVariable final Long id) {
        LibraryEntryResponse response = libraryEntryService.findLibraryEntry(id);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    @Operation(summary = "Creates new library entry, returns newly created library entry id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Newly created library entry id",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Long.class))}),})
    public ResponseEntity<?> createLibraryEntry(@RequestBody @Valid final CreateLibraryEntryRequest libraryEntryRequest) {
        Long authorId = libraryEntryService.createLibraryEntry(libraryEntryRequest);
        return ResponseEntity.status(CREATED).body(authorId);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Deletes selected libraryEntry")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "libraryEntry deleted successfully",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation =  Long.class))}),
    })
    public ResponseEntity<?> deleteLibraryEntry(@PathVariable final Long id){
        libraryEntryService.deleteLibraryEntry(id);
        return ResponseEntity.status(ACCEPTED).build();
    }
}
