package pl.o4b.web.controller.paths;

public class Paths {
    public static final String AUTHORS = "authors";
    public static final String LANGUAGES = "/languages";
    public static final String LIBRARY_ENTRIES = "library-entries";
    public static final String LIBRARIES = "libraries";
    public static final String LIBRARY_GROUPS = "library-groups";
    public static final String BOOKS = "books";
    public static final String USERS = "users";
    public static final String BORROW = "borrow";
    public static final String BORROW_FROM_ALL = "borrowFromAll";
    public static final String RETURN = "return";
    public static final String BLOCK = "block";
    public static final String UNBLOCK = "unblock";
    public static final String SAMPLE = "sample";
}
