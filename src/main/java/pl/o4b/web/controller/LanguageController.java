package pl.o4b.web.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.o4b.service.AuthorService;
import pl.o4b.service.LanguageService;
import pl.o4b.web.dto.request.CreateAuthorRequest;
import pl.o4b.web.dto.request.CreateLanguageRequest;
import pl.o4b.web.dto.response.AuthorResponse;
import pl.o4b.web.dto.response.LanguageResponse;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static pl.o4b.web.controller.paths.Paths.AUTHORS;
import static pl.o4b.web.controller.paths.Paths.LANGUAGES;

@RestController
@RequestMapping(LANGUAGES)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
class LanguageController {
    private final LanguageService languageService;

    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    @Operation(summary = "Returns info about Language with given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Language found",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = LanguageResponse.class))}),
            @ApiResponse(responseCode = "404", description = "Language not found",
                    content = @Content)
    })
    public ResponseEntity<?> findLanguage(@PathVariable final Long id) {
        LanguageResponse response = languageService.findLanguage(id);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    @Operation(summary = "Creates new author, returns newly created Language id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Newly created Language id",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Long.class))}),})
    public ResponseEntity<?> createLanguage(@RequestBody @Valid final CreateLanguageRequest languageRequest) {
        Long languageId = languageService.createLanguage(languageRequest);
        return ResponseEntity.status(CREATED).body(languageId);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Deletes selected Language")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Language deleted successfully",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation =  Long.class))}),
    })
    public ResponseEntity<?> deleteLanguage(@PathVariable final Long id){
        languageService.deleteLanguage(id);
        return ResponseEntity.status(ACCEPTED).build();
    }
}
