package pl.o4b.web.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.o4b.service.AuthorService;
import pl.o4b.web.dto.request.CreateAuthorRequest;
import pl.o4b.web.dto.response.AuthorResponse;
import pl.o4b.web.dto.response.UserResponse;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static pl.o4b.web.controller.paths.Paths.AUTHORS;

@RestController
@RequestMapping(AUTHORS)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
class AuthorController {
    private final AuthorService authorService;

    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    @Operation(summary = "Returns info about author with given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Author found",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = AuthorResponse.class))}),
            @ApiResponse(responseCode = "404", description = "Author not found",
                    content = @Content)
    })
    public ResponseEntity<?> findAuthor(@PathVariable final Long id) {
        AuthorResponse response = authorService.findAuthor(id);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    @Operation(summary = "Creates new author, returns newly created author id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Newly created author id",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Long.class))}),})
    public ResponseEntity<?> createAuthor(@RequestBody @Valid final CreateAuthorRequest authorRequest) {
        Long authorId = authorService.createAuthor(authorRequest);
        return ResponseEntity.status(CREATED).body(authorId);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Deletes selected author")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Author deleted successfully",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation =  Long.class))}),
    })
    public ResponseEntity<?> deleteAuthor(@PathVariable final Long id){
        authorService.deleteAuthor(id);
        return ResponseEntity.status(ACCEPTED).build();
    }
}
