package pl.o4b.web.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.o4b.db.model.book.Author;
import pl.o4b.db.model.book.Book;
import pl.o4b.db.repository.LibraryEntryRepository;
import pl.o4b.db.repository.LibraryGroupRepository;
import pl.o4b.db.repository.LibraryRepository;
import pl.o4b.db.repository.UserRepository;
import pl.o4b.db.repository.book.AuthorRepository;
import pl.o4b.db.repository.book.BookRepository;
import pl.o4b.db.repository.book.LanguageRepository;
import pl.o4b.service.*;
import pl.o4b.web.controller.paths.Paths;
import pl.o4b.web.dto.request.*;
import utils.ISBNUtils;
import utils.IdGenerator;
import utils.SampleDataNames;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static pl.o4b.web.controller.paths.Paths.BORROW_FROM_ALL;

@RestController
@RequestMapping(Paths.SAMPLE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class SampleDataController {

    UserService userService;
    BookService bookService;
    LibraryEntryService libraryEntryService;
    LibraryService libraryService;
    LibraryGroupService libraryGroupService;

    @Operation(summary = "Load sample data.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Sample data loaded.",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation =  Long.class))}),
    })
    @PutMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> loadSampleData(){
        loadSampleBooks();
        loadSampleLibraryEntries();
        loadSampleLibraries();
        loadSampleLibraryGroups();
        loadSampleUsers();
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    private void loadSampleBooks(){
        int loadedBooks = 1;
        int offset = 100000;
        List<Book> books = new ArrayList<>();
        while (loadedBooks <= 100){
            CreateBookRequest request = new CreateBookRequest(
                    SampleDataNames.BOOK + loadedBooks,
                    ISBNUtils.generateISBN(),
                    IdGenerator.generateRandomIDList(offset),
                    IdGenerator.generateRandomID(offset),
                    new Date()
            );
            bookService.createBook(request);
            loadedBooks++;
        }
    }

    private void loadSampleLibraryEntries(){
        int loadedEntries = 1;
        int offset = 1;
        while(loadedEntries <= 100){
            CreateLibraryEntryRequest request = new CreateLibraryEntryRequest(SampleDataNames.ENTRY,
                    5,
                    IdGenerator.generateRandomID(offset)
            );
            libraryEntryService.createLibraryEntry(request);
            loadedEntries++;
        }
    }

    private void loadSampleLibraries(){
        int loadedLibraries = 1;
        int offset = 1;
        while(loadedLibraries <= 100){
            CreateLibraryRequest request = new CreateLibraryRequest(SampleDataNames.LIBRARY,
                    IdGenerator.generateRandomIDList(offset)
            );
            libraryService.createLibrary(request);
            loadedLibraries++;
        }
    }

    private void loadSampleLibraryGroups(){
        int loadedGroups = 1;
        int offset = 1;
        while(loadedGroups <= 100){
            CreateLibraryGroupRequest request = new CreateLibraryGroupRequest(SampleDataNames.GROUP,
                    IdGenerator.generateRandomIDList(offset)
            );
            libraryGroupService.createLibraryGroup(request);
            loadedGroups++;
        }
    }

    private void loadSampleUsers(){
        int loadedUsers = 1;
        int offset1 = 1;
        int offset2 = 100000;
        while(loadedUsers <= 100){
            CreateUserRequest request = new CreateUserRequest(Long.valueOf(loadedUsers +1 ),
                    SampleDataNames.NAME,
                    SampleDataNames.SURNAME,
                    SampleDataNames.EMAIL_PREFIX+loadedUsers+SampleDataNames.EMAIL_SUFFIX,
                    IdGenerator.generateRandomID(offset2),
                    IdGenerator.generateRandomID(offset1)
            );
            userService.createUser(request);
            loadedUsers++;
        }
    }

}
