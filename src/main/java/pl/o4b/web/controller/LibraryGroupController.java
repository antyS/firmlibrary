package pl.o4b.web.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.o4b.db.model.library.Library;
import pl.o4b.db.model.library.LibraryGroup;
import pl.o4b.service.LibraryGroupService;
import pl.o4b.service.LibraryService;
import pl.o4b.web.dto.request.CreateLibraryGroupRequest;
import pl.o4b.web.dto.request.CreateLibraryRequest;
import pl.o4b.web.dto.response.LibraryGroupResponse;
import pl.o4b.web.dto.response.LibraryResponse;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static pl.o4b.web.controller.paths.Paths.LIBRARIES;
import static pl.o4b.web.controller.paths.Paths.LIBRARY_GROUPS;

@RestController
@RequestMapping(LIBRARY_GROUPS)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
class LibraryGroupController {
    private final LibraryGroupService libraryGroupService;

    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    @Operation(summary = "Returns info about library group with given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Library group found",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Library.class))}),
            @ApiResponse(responseCode = "404", description = "Library group not found",
                    content = @Content)
    })
    public ResponseEntity<?> findLibraryGroup(@PathVariable final Long id) {
        LibraryGroupResponse response = libraryGroupService.findLibraryGroup(id);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    @Operation(summary = "Creates new library group, returns newly created library group id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Newly created library group id",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Long.class))}),})
    public ResponseEntity<?> createLibraryGroup(@RequestBody @Valid final CreateLibraryGroupRequest libraryGroupRequest) {
        Long libraryGroupId = libraryGroupService.createLibraryGroup(libraryGroupRequest);
        return ResponseEntity.status(CREATED).body(libraryGroupId);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Deletes selected library group")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "library group deleted successfully",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation =  Long.class))}),
    })
    public ResponseEntity<?> deleteLibraryGroup(@PathVariable final Long id){
        libraryGroupService.deleteLibraryGroup(id);
        return ResponseEntity.status(ACCEPTED).build();
    }
}
