package pl.o4b.web.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.o4b.db.model.library.Library;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.service.LibraryEntryService;
import pl.o4b.service.LibraryService;
import pl.o4b.web.dto.request.CreateLibraryEntryRequest;
import pl.o4b.web.dto.request.CreateLibraryRequest;
import pl.o4b.web.dto.response.LibraryEntryResponse;
import pl.o4b.web.dto.response.LibraryResponse;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static pl.o4b.web.controller.paths.Paths.AUTHORS;
import static pl.o4b.web.controller.paths.Paths.LIBRARIES;

@RestController
@RequestMapping(LIBRARIES)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
class LibraryController {
    private final LibraryService libraryService;

    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    @Operation(summary = "Returns info about library with given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Library found",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Library.class))}),
            @ApiResponse(responseCode = "404", description = "Library not found",
                    content = @Content)
    })
    public ResponseEntity<?> findLibrary(@PathVariable final Long id) {
        LibraryResponse response = libraryService.findLibrary(id);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    @Operation(summary = "Creates new library, returns newly created library id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Newly created library id",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = Long.class))}),})
    public ResponseEntity<?> createLibrary(@RequestBody @Valid final CreateLibraryRequest libraryRequest) {
        Long authorId = libraryService.createLibrary(libraryRequest);
        return ResponseEntity.status(CREATED).body(authorId);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Deletes selected library")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "library deleted successfully",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation =  Long.class))}),
    })
    public ResponseEntity<?> deleteLibrary(@PathVariable final Long id){
        libraryService.deleteLibrary(id);
        return ResponseEntity.status(ACCEPTED).build();
    }
}
