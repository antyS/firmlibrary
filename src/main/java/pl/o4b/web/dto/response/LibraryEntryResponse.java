package pl.o4b.web.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import pl.o4b.db.model.book.Book;

import java.util.List;

@Data
@JsonInclude(content = JsonInclude.Include.NON_NULL)
public class LibraryEntryResponse extends SystemResponse {

    public Long id;
    public String name;
    public Integer amount;
    public Long bookId;
}
