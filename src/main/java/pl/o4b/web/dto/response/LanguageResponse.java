package pl.o4b.web.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(content = JsonInclude.Include.NON_NULL)
public class LanguageResponse extends SystemResponse {

    public Long id;
    public String name;
}
