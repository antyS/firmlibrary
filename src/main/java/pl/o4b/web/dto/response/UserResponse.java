package pl.o4b.web.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import pl.o4b.db.model.user.Status;

@Data
@JsonInclude(content = JsonInclude.Include.NON_NULL)
public class UserResponse extends SystemResponse {

    public Long id;
    public String name;
    public String surname;
    public String email;
    public Status status;
    public Long firmId;
    public Long libraryId;

}
