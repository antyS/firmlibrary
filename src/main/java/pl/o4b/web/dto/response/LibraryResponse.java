package pl.o4b.web.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(content = JsonInclude.Include.NON_NULL)
public class LibraryResponse extends SystemResponse {

    public Long id;
    public String name;
    public List<Long> libraryEntryIds;
}
