package pl.o4b.web.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import pl.o4b.db.model.book.Author;
import pl.o4b.db.model.book.Language;

import java.util.Date;
import java.util.List;

@Data
@JsonInclude(content = JsonInclude.Include.NON_NULL)
public class BookResponse extends SystemResponse {
    public List<Author> authors;
    public Long id;
    public Language lang;
    public String title;
    public Date published;
    @Schema(example = "9783161484100")
    public String isbn;
}
