package pl.o4b.web.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import pl.o4b.db.model.user.Status;

import java.util.Date;

@Data
@JsonInclude(content = JsonInclude.Include.NON_NULL)
public class BorrowResponse extends SystemResponse {

    public Long id;
    public Date borrowDate;
    public Date expectedReturnDate;
    public Long userId;
    public Long libraryEntryId;

}
