package pl.o4b.web.dto.mapper;

import org.mapstruct.*;
import pl.o4b.db.model.book.Book;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.web.dto.request.CreateLibraryEntryRequest;
import pl.o4b.web.dto.response.LibraryEntryResponse;

import java.util.List;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LibraryEntryMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
    })
    LibraryEntry toLibraryEntry(CreateLibraryEntryRequest request, Book book);

    @Mappings({
            @Mapping(source = "book.id", target = "bookId")
    })
    LibraryEntryResponse toLibraryEntryResponse(LibraryEntry libraryEntry);
}
