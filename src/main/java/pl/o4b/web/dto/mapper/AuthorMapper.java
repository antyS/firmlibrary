package pl.o4b.web.dto.mapper;

import org.mapstruct.*;
import pl.o4b.db.model.book.Author;
import pl.o4b.web.dto.response.AuthorResponse;
import pl.o4b.web.dto.request.CreateAuthorRequest;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AuthorMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
    })
    Author toAuthor(CreateAuthorRequest request);

    AuthorResponse toAuthorResponse(Author author);
}
