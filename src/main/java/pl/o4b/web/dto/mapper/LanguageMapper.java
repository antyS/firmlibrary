package pl.o4b.web.dto.mapper;

import org.mapstruct.*;
import pl.o4b.db.model.book.Author;
import pl.o4b.db.model.book.Language;
import pl.o4b.web.dto.request.CreateAuthorRequest;
import pl.o4b.web.dto.request.CreateLanguageRequest;
import pl.o4b.web.dto.response.AuthorResponse;
import pl.o4b.web.dto.response.LanguageResponse;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LanguageMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
    })
    Language toLanguage(CreateLanguageRequest request);

    LanguageResponse toLanguageResponse(Language language);
}
