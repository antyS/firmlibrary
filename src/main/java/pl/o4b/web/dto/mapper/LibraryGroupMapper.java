package pl.o4b.web.dto.mapper;

import org.mapstruct.*;
import pl.o4b.db.model.library.Library;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.db.model.library.LibraryGroup;
import pl.o4b.web.dto.request.CreateLibraryGroupRequest;
import pl.o4b.web.dto.request.CreateLibraryRequest;
import pl.o4b.web.dto.response.LibraryGroupResponse;
import pl.o4b.web.dto.response.LibraryResponse;

import java.util.List;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LibraryGroupMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
    })
    LibraryGroup toLibraryGroup(CreateLibraryGroupRequest request, List<Library> libraries);

    @Mappings({
            @Mapping(source = "libraryGroup.libraryIds", target = "libraryIds")
    })
    LibraryGroupResponse toLibraryGroupResponse(LibraryGroup libraryGroup);
}
