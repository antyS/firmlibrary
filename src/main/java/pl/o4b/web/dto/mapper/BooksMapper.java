package pl.o4b.web.dto.mapper;

import org.mapstruct.*;
import pl.o4b.db.model.book.*;
import pl.o4b.web.dto.request.CreateBookRequest;
import pl.o4b.web.dto.response.BookResponse;

import java.util.List;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BooksMapper {


    @Mappings({
            @Mapping(target = "id", ignore = true),
    })
    Book toBook(CreateBookRequest request, Language lang, List<Author> authors);

    BookResponse toBookResponse(Book book);
}
