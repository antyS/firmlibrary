package pl.o4b.web.dto.mapper;


import org.mapstruct.*;
import pl.o4b.db.model.firm.Firm;
import pl.o4b.db.model.library.Library;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.db.model.user.User;
import pl.o4b.web.dto.request.CreateUserRequest;
import pl.o4b.web.dto.response.UserResponse;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(source = "request.name",target = "name"),
    })
    User toUser(CreateUserRequest request, Firm firm, Library library);

    @Mappings({
            @Mapping(source = "firm.id", target = "firmId"),
            @Mapping(source = "library.id", target = "libraryId")
    })
    UserResponse toUserResponse(User user);

}
