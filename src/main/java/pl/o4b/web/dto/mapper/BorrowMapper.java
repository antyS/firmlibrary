package pl.o4b.web.dto.mapper;


import org.mapstruct.*;
import pl.o4b.db.model.Borrow;
import pl.o4b.db.model.firm.Firm;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.db.model.user.User;
import pl.o4b.web.dto.request.CreateBorrowRequest;
import pl.o4b.web.dto.request.CreateUserRequest;
import pl.o4b.web.dto.response.BorrowResponse;
import pl.o4b.web.dto.response.UserResponse;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BorrowMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
    })
    Borrow toBorrow(CreateBorrowRequest request, User user, LibraryEntry libraryEntry);

    @Mappings({
            @Mapping(source = "user.id", target = "userId"),
            @Mapping(source = "libraryEntry.id", target = "libraryEntryId")
    })
    BorrowResponse toBorrowResponse(Borrow borrow);

}
