package pl.o4b.web.dto.mapper;

import org.mapstruct.*;
import pl.o4b.db.model.book.Book;
import pl.o4b.db.model.library.Library;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.web.dto.request.CreateLibraryEntryRequest;
import pl.o4b.web.dto.request.CreateLibraryRequest;
import pl.o4b.web.dto.response.LibraryEntryResponse;
import pl.o4b.web.dto.response.LibraryResponse;

import java.util.List;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LibraryMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
    })
    Library toLibrary(CreateLibraryRequest request, List<LibraryEntry> libraryEntries);

    @Mappings({
            @Mapping(source = "library.libraryEntryIds", target = "libraryEntryIds")
    })
    LibraryResponse toLibraryResponse(Library library);
}
