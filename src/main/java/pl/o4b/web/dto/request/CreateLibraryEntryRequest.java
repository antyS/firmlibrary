package pl.o4b.web.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.annotation.Nonnegative;
import javax.annotation.Nullable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class CreateLibraryEntryRequest {
    @NotBlank
    @Schema(example = "Książki podarowane przez Baracka Obamę")
    public String name;

    @Positive
    @Schema(example = "2")
    public Integer amount;

    @Nullable
    @Nonnegative
    @Schema(example = "1")
    public Long bookId;

}
