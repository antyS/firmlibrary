package pl.o4b.web.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.annotation.Nonnegative;
import javax.annotation.Nullable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
public class CreateBorrowRequest {

    @Nonnegative
    @NotNull
    @Schema(example = "1")
    public Long userId;

    @Nonnegative
    @NotNull
    @Schema(example = "1")
    public Long libraryEntryId;

    @Temporal(TemporalType.TIMESTAMP)
    public Date borrowDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Future
    @Schema(example = "2020-06-21T21:11:09.955Z")
    public Date expectedReturnDate;


}
