package pl.o4b.web.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Builder
public class CreateLanguageRequest {
    @NotBlank
    @Schema(example = "Polski")
    public String name;
}
