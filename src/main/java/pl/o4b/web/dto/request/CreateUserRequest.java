package pl.o4b.web.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.annotation.Nonnegative;
import javax.annotation.Nullable;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@Builder
@AllArgsConstructor
public class CreateUserRequest {

    @Nullable
    @Schema(example = "1")
    public Long id;

    @NotBlank
    @Schema(example = "Janina")
    public String name;

    @NotBlank
    @Schema(example = "Kołolska")
    public String surname;

    @NotBlank
    @Email
    @Schema(example = "Kololska@gmail.com")
    public String email;

    @Nonnegative
    @NotNull
    @Schema(example = "100000")
    public Long firmId;

    @Nonnegative
    @NotNull
    @Schema(example = "1")
    public Long libraryId;


}
