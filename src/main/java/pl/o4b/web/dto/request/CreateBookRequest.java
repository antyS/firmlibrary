package pl.o4b.web.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.UniqueElements;
import pl.o4b.validation.ISBN10or13;

import javax.annotation.Nonnegative;
import javax.validation.constraints.*;
import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class CreateBookRequest {

    @NotBlank
    @Schema(example = "Bilble")
    public String title;

    /**
     * Always saved as only digits isbn13
     */
    @NotBlank
    @Schema(example = "9783161484100")
    public String isbn;

    @NotEmpty
    @UniqueElements
    @Schema(example = "[100000]")
    List<@NotNull Long> authorIds;

    @Nonnegative
    @NotNull
    @Schema(example = "100000")
    public Long langId;

    @Past
    @NotNull
    public Date published;


}
