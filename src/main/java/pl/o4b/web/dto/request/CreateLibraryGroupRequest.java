package pl.o4b.web.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.UniqueElements;

import javax.annotation.Nullable;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class CreateLibraryGroupRequest {
    @NotBlank
    @Schema(example = "Biblioteki Microsoftu")
    public String name;

    @Nullable
    @UniqueElements
    @Schema(example = "[1,2]")
    public List<Long> libraryIds;

}
