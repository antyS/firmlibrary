package pl.o4b.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.o4b.db.model.book.Book;
import pl.o4b.db.model.library.Library;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.db.model.library.LibraryGroup;
import pl.o4b.db.repository.LibraryEntryRepository;
import pl.o4b.db.repository.LibraryRepository;
import pl.o4b.db.repository.book.BookRepository;
import pl.o4b.exception.*;
import pl.o4b.web.dto.mapper.LibraryEntryMapper;
import pl.o4b.web.dto.mapper.LibraryMapper;
import pl.o4b.web.dto.request.CreateLibraryEntryRequest;
import pl.o4b.web.dto.request.CreateLibraryRequest;
import pl.o4b.web.dto.response.LibraryEntryResponse;
import pl.o4b.web.dto.response.LibraryGroupResponse;
import pl.o4b.web.dto.response.LibraryResponse;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class LibraryService {
    LibraryRepository libraryRepository;
    LibraryMapper libraryMapper;
    LibraryEntryRepository libraryEntryRepository;

    public Long createLibrary(final CreateLibraryRequest libraryRequest) {
        List<Long> entryIds = new ArrayList<>(libraryRequest.getLibraryEntryIds());
        List<LibraryEntry> libraryEntries = new ArrayList<>();
        for( Long id: entryIds){
            libraryEntries.add(libraryEntryRepository.findById(id).orElseThrow(NoSuchLibraryEntryExeption::new));
        }
        final Library mappedLibrary = libraryMapper.toLibrary(libraryRequest, libraryEntries);
        final Library savedLibrary = libraryRepository.save(mappedLibrary);
        return savedLibrary.getId();
    }

    public LibraryResponse findLibrary(final Long id) {
        final Library library = libraryRepository.findById(id).orElseThrow(NoSuchLibraryExeption::new);
        return libraryMapper.toLibraryResponse(library);
    }

    public void deleteLibrary(final Long id){
        final Library library = libraryRepository.findById(id).orElseThrow(NoSuchAuthorException::new);
        libraryRepository.delete(library);
    }

    public List<LibraryResponse> getAll(){
        List<LibraryResponse> responseList = new ArrayList<>();
        libraryRepository.findAll().forEach(libraryGroup -> {responseList.add(libraryMapper.toLibraryResponse(libraryGroup));});
        return responseList;
    }

}
