package pl.o4b.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.o4b.db.model.Borrow;
import pl.o4b.db.model.firm.Firm;
import pl.o4b.db.model.library.Library;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.db.model.user.Status;
import pl.o4b.db.model.user.User;
import pl.o4b.db.repository.*;
import pl.o4b.exception.*;
import pl.o4b.web.dto.mapper.BorrowMapper;
import pl.o4b.web.dto.mapper.UserMapper;
import pl.o4b.web.dto.request.CreateBorrowRequest;
import pl.o4b.web.dto.request.CreateUserRequest;
import pl.o4b.web.dto.response.BorrowResponse;
import pl.o4b.web.dto.response.UserResponse;

@Service
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class BorrowService {
    BorrowRepository borrowRepository;
    UserRepository userRepository;
    BorrowMapper borrowMapper;
    LibraryEntryRepository libraryEntryRepository;

    public Long createBorrow(final CreateBorrowRequest createBorrowRequest) {
        final User user = userRepository.findById(createBorrowRequest.getUserId()).orElseThrow(UserNotFoundException::new);
        final LibraryEntry libraryEntry = libraryEntryRepository.findById(createBorrowRequest.libraryEntryId).orElseThrow(NoSuchLibraryEntryExeption::new);
        final Borrow borrow = borrowMapper.toBorrow(createBorrowRequest, user, libraryEntry);
        final Borrow savedBorrow = borrowRepository.save(borrow);
        return savedBorrow.getId();
    }

    public void deleteBorrow(final Long id){
        final User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        final Borrow borrow = borrowRepository.findByUser(user).get(0);
        borrowRepository.delete(borrow);
    }

}
