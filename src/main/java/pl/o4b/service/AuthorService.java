package pl.o4b.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.o4b.db.model.book.Author;
import pl.o4b.db.repository.book.AuthorRepository;
import pl.o4b.exception.NoSuchAuthorException;
import pl.o4b.web.dto.mapper.AuthorMapper;
import pl.o4b.web.dto.request.CreateAuthorRequest;
import pl.o4b.web.dto.response.AuthorResponse;

@Service
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class AuthorService {
    AuthorRepository authorRepository;
    AuthorMapper authorMapper;

    public Long createAuthor(final CreateAuthorRequest authorRequest) {
        final Author mappedAuthor = authorMapper.toAuthor(authorRequest);
        final Author savedAuthor = authorRepository.save(mappedAuthor);
        return savedAuthor.getId();
    }

    public AuthorResponse findAuthor(final Long id) {
        final Author author = authorRepository.findById(id).orElseThrow(NoSuchAuthorException::new);
        return authorMapper.toAuthorResponse(author);
    }

    public void deleteAuthor(final Long id){
        final Author author = authorRepository.findById(id).orElseThrow(NoSuchAuthorException::new);
        authorRepository.delete(author);
    }

}
