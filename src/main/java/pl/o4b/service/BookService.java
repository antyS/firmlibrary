package pl.o4b.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.o4b.db.model.book.*;
import pl.o4b.db.repository.book.*;
import pl.o4b.exception.DuplicateIsbnException;
import pl.o4b.exception.NoSuchAuthorException;
import pl.o4b.exception.NoSuchBookExeption;
import pl.o4b.exception.NoSuchLanguageException;
import pl.o4b.web.dto.mapper.BooksMapper;
import pl.o4b.web.dto.request.CreateBookRequest;
import pl.o4b.web.dto.response.BookResponse;
import utils.ISBNUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class BookService {

    BookRepository bookRepository;
    AuthorRepository authorRepository;
    LanguageRepository languageRepository;
    BooksMapper booksMapper;

    @Transactional
    public Long createBook(final CreateBookRequest bookRequest) {
        final String normalizedISBN = ISBNUtils.normalizeISBN(bookRequest.getIsbn());
        bookRequest.setIsbn(normalizedISBN);

        if (bookRepository.existsByIsbn(bookRequest.getIsbn())) {
            throw new DuplicateIsbnException();
        }

        final List<Author> authors = fetchAuthors(bookRequest.getAuthorIds());
        final Language language = languageRepository.findById(bookRequest.getLangId())
                .orElseThrow(NoSuchLanguageException::new);

        final Book book = booksMapper.toBook(bookRequest, language, authors);

        final Book savedBook = bookRepository.save(book);
        return savedBook.getId();
    }


    private List<Author> fetchAuthors(final List<Long> authorIds) {
        final Iterable<Author> authorIterable = authorRepository.findAllById(authorIds);
        final List<Author> authors = new ArrayList<>();
        authorIds.forEach( id -> authors.add(authorRepository.findById(id).orElseThrow(NoSuchAuthorException::new)));
        return authors;
    }

    public BookResponse findBook(final Long id){
        final Book book = bookRepository.findById(id).orElseThrow(NoSuchBookExeption::new);
        return booksMapper.toBookResponse(book);
    }

    public void deleteBook(final Long id){
        final Book book = bookRepository.findById(id).orElseThrow(NoSuchBookExeption::new);
        bookRepository.delete(book);
    }

    public List<BookResponse> getAll(){
        List<BookResponse> responses = new ArrayList<>();
        bookRepository.findAll().forEach(book -> responses.add(booksMapper.toBookResponse(book)));
        return responses;
    }

}
