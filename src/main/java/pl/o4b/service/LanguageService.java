package pl.o4b.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.o4b.db.model.book.Author;
import pl.o4b.db.model.book.Language;
import pl.o4b.db.repository.book.AuthorRepository;
import pl.o4b.db.repository.book.LanguageRepository;
import pl.o4b.exception.NoSuchAuthorException;
import pl.o4b.exception.NoSuchLanguageException;
import pl.o4b.web.dto.mapper.AuthorMapper;
import pl.o4b.web.dto.mapper.LanguageMapper;
import pl.o4b.web.dto.request.CreateAuthorRequest;
import pl.o4b.web.dto.request.CreateLanguageRequest;
import pl.o4b.web.dto.response.AuthorResponse;
import pl.o4b.web.dto.response.LanguageResponse;

@Service
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class LanguageService {
    LanguageRepository languageRepository;
    LanguageMapper languageMapper;

    public Long createLanguage(final CreateLanguageRequest languageRequest) {
        final Language mappedLanguage = languageMapper.toLanguage(languageRequest);
        final Language savedLanguage = languageRepository.save(mappedLanguage);
        return savedLanguage.getId();
    }

    public LanguageResponse findLanguage(final Long id) {
        final Language language = languageRepository.findById(id).orElseThrow(NoSuchLanguageException::new);
        return languageMapper.toLanguageResponse(language);
    }

    public void deleteLanguage(final Long id){
        final Language language = languageRepository.findById(id).orElseThrow(NoSuchLanguageException::new);
        languageRepository.delete(language);
    }

}
