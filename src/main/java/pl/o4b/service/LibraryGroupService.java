package pl.o4b.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.o4b.db.model.library.Library;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.db.model.library.LibraryGroup;
import pl.o4b.db.repository.LibraryEntryRepository;
import pl.o4b.db.repository.LibraryGroupRepository;
import pl.o4b.db.repository.LibraryRepository;
import pl.o4b.exception.NoSuchAuthorException;
import pl.o4b.exception.NoSuchLibraryEntryExeption;
import pl.o4b.exception.NoSuchLibraryExeption;
import pl.o4b.exception.NoSuchLibraryGroupExeption;
import pl.o4b.web.dto.mapper.LibraryGroupMapper;
import pl.o4b.web.dto.mapper.LibraryMapper;
import pl.o4b.web.dto.request.CreateLibraryGroupRequest;
import pl.o4b.web.dto.request.CreateLibraryRequest;
import pl.o4b.web.dto.response.LibraryGroupResponse;
import pl.o4b.web.dto.response.LibraryResponse;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class LibraryGroupService {
    LibraryGroupRepository libraryGroupRepository;
    LibraryGroupMapper libraryGroupMapper;
    LibraryRepository libraryRepository;

    public Long createLibraryGroup(final CreateLibraryGroupRequest libraryRequest) {
        List<Long> libraryIds = new ArrayList<>(libraryRequest.getLibraryIds());
        List<Library> libraries = new ArrayList<>();
        for( Long id: libraryIds){
            libraries.add(libraryRepository.findById(id).orElseThrow(NoSuchLibraryExeption::new));
        }
        final LibraryGroup mappedLibraryGroup = libraryGroupMapper.toLibraryGroup(libraryRequest, libraries);
        final LibraryGroup savedLibraryGroup = libraryGroupRepository.save(mappedLibraryGroup);
        return savedLibraryGroup.getId();
    }

    public LibraryGroupResponse findLibraryGroup(final Long id) {
        final LibraryGroup libraryGroup = libraryGroupRepository.findById(id).orElseThrow(NoSuchLibraryExeption::new);
        return libraryGroupMapper.toLibraryGroupResponse(libraryGroup);
    }

    public List<LibraryGroupResponse> getAll(){
        List<LibraryGroupResponse> responseList = new ArrayList<>();
        libraryGroupRepository.findAll().forEach(libraryGroup -> {responseList.add(libraryGroupMapper.toLibraryGroupResponse(libraryGroup));});
        return responseList;
    }

    public void deleteLibraryGroup(final Long id){
        final LibraryGroup libraryGroup = libraryGroupRepository.findById(id).orElseThrow(NoSuchLibraryGroupExeption::new);
        libraryGroupRepository.delete(libraryGroup);
    }

}
