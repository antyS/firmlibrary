package pl.o4b.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.o4b.db.model.book.Book;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.db.repository.LibraryEntryRepository;
import pl.o4b.db.repository.book.BookRepository;
import pl.o4b.exception.*;
import pl.o4b.web.dto.mapper.LibraryEntryMapper;
import pl.o4b.web.dto.request.CreateLibraryEntryRequest;
import pl.o4b.web.dto.response.LibraryEntryResponse;

@Service
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class LibraryEntryService {
    LibraryEntryRepository libraryEntryRepository;
    BookRepository bookRepository;
    LibraryEntryMapper libraryEntryMapper;

    public Long createLibraryEntry(final CreateLibraryEntryRequest libraryRequest) {
        final Book book = bookRepository.findById(libraryRequest.bookId).orElseThrow(NoSuchBookExeption::new);
        final LibraryEntry mappedLibraryEntry = libraryEntryMapper.toLibraryEntry(libraryRequest, book);
        final LibraryEntry savedLibraryEntry = libraryEntryRepository.save(mappedLibraryEntry);
        return savedLibraryEntry.getId();
    }

    public LibraryEntryResponse findLibraryEntry(final Long id) {
        final LibraryEntry libraryEntry = libraryEntryRepository.findById(id).orElseThrow(NoSuchLibraryEntryExeption::new);
        return libraryEntryMapper.toLibraryEntryResponse(libraryEntry);
    }

    public void deleteLibraryEntry(final Long id){
        final LibraryEntry libraryEntry = libraryEntryRepository.findById(id).orElseThrow(NoSuchLibraryEntryExeption::new);
        libraryEntryRepository.delete(libraryEntry);
    }

    public Boolean borrowBook(final Long libraryEntryId, final Long bookId){
        final LibraryEntry libraryEntry = libraryEntryRepository.findById(libraryEntryId).orElseThrow(NoSuchLibraryEntryExeption::new);
        if(libraryEntry.book.id.equals(bookId)) {
            if (libraryEntry.amount > 0) {
                libraryEntry.amount -= 1;
                libraryEntryRepository.save(libraryEntry);
                return true;
            } else {
                throw new NoMoreBooksException();
            }
        }
        return false;
    }

    public void returnBook(final Long entryId, final Long bookId){
        final LibraryEntry libraryEntry = libraryEntryRepository.findById(entryId).orElseThrow(NoSuchLibraryEntryExeption::new);
        if(libraryEntry.book.id.equals(bookId)) {
            libraryEntry.amount += 1;
            libraryEntryRepository.save(libraryEntry);
        }
    }

}
