package pl.o4b.service;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.o4b.db.model.firm.Firm;
import pl.o4b.db.model.library.Library;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.db.model.user.Status;
import pl.o4b.db.model.user.User;
import pl.o4b.db.repository.FirmRepository;
import pl.o4b.db.repository.LibraryEntryRepository;
import pl.o4b.db.repository.LibraryRepository;
import pl.o4b.db.repository.UserRepository;
import pl.o4b.exception.*;
import pl.o4b.web.dto.request.CreateUserRequest;
import pl.o4b.web.dto.response.UserResponse;
import pl.o4b.web.dto.mapper.UserMapper;

@Service
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class UserService {
    UserRepository userRepository;
    UserMapper userMapper;
    FirmRepository firmRepository;
    LibraryRepository libraryRepository;

    public UserResponse findUser(final Long id) {
        return userRepository.findById(id)
                .map(userMapper::toUserResponse)
                .orElseThrow(UserNotFoundException::new);
    }

    public Long createUser(final CreateUserRequest createUserRequest) {
        if (userRepository.existsByEmail(createUserRequest.getEmail())) {
            throw new DuplicateEmailException();
        }
        final Firm firm = firmRepository.findById(createUserRequest.getFirmId()).orElseThrow(NoSuchFirmExeption::new);
        final Library library = libraryRepository.findById(createUserRequest.libraryId).orElseThrow(NoSuchLibraryExeption::new);
        final User user = userMapper.toUser(createUserRequest, firm, library);
        user.setStatus(Status.ACTIVE);
        final User savedUser = userRepository.save(user);
        return savedUser.getId();
    }

    public Long createUser(final CreateUserRequest createUserRequest, Long explicitID) {
        if (userRepository.existsByEmail(createUserRequest.getEmail())) {
            throw new DuplicateEmailException();
        }
        final Firm firm = firmRepository.findById(createUserRequest.getFirmId()).orElseThrow(NoSuchFirmExeption::new);
        final Library library = libraryRepository.findById(createUserRequest.libraryId).orElseThrow(NoSuchLibraryExeption::new);
        final User user = userMapper.toUser(createUserRequest, firm, library);
        user.setStatus(Status.ACTIVE);
        user.setId(explicitID);
        final User savedUser = userRepository.save(user);
        return savedUser.getId();
    }

    public void blockUser(final Long id) {
        final User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        user.setStatus(Status.BLOCKED);
        userRepository.save(user);
    }

    public void unblockUser(final Long id) {
        final User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        user.setStatus(Status.ACTIVE);
        userRepository.save(user);
    }

    public void deleteUser(final Long id){
        final User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        userRepository.delete(user);
    }

    public Long createOrUpdateUser(final CreateUserRequest userRequest) {
        if (userRequest.getId() == null) {
            throw new NullIdException();
        } else {
            if(userRepository.findById(userRequest.getId()).isPresent()){
                User selectedUser = userRepository.findById(userRequest.getId()).get();
                Firm firm = firmRepository.findById(userRequest.firmId).orElseThrow(NoSuchFirmExeption::new);
                Library library = libraryRepository.findById(userRequest.libraryId).orElseThrow(NoSuchLibraryExeption::new);
                User updatedUser = userMapper.toUser(userRequest, firm, library);
                selectedUser.setName(updatedUser.name);
                selectedUser.setEmail(updatedUser.email);
                selectedUser.setSurname(updatedUser.surname);
                selectedUser.setFirm(updatedUser.firm);
                selectedUser.setLibrary(updatedUser.library);
                return userRepository.save(selectedUser).getId();
            } else {
                return createUser(userRequest, userRequest.getId());
            }
        }
    }
}
