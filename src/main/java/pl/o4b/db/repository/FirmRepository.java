package pl.o4b.db.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.o4b.db.model.firm.Firm;

@Repository
public interface FirmRepository extends CrudRepository<Firm, Long> {

}
