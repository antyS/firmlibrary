package pl.o4b.db.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.o4b.db.model.Borrow;
import pl.o4b.db.model.user.User;

import java.util.List;

@Repository
public interface BorrowRepository extends CrudRepository<Borrow, Long> {
    List<Borrow> findByUser(User user);
}



