package pl.o4b.db.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.o4b.db.model.library.Library;

@Repository
public interface LibraryRepository extends CrudRepository<Library, Long> {

}



