package pl.o4b.db.repository.book;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.o4b.db.model.book.Language;

@Repository
public interface LanguageRepository extends CrudRepository<Language, Long> {

}
