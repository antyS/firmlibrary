package pl.o4b.db.repository.book;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.o4b.db.model.book.Author;

@Repository
public interface AuthorRepository extends CrudRepository<Author, Long> {

}