package pl.o4b.db.repository.book;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.o4b.db.model.book.Book;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {
    boolean existsByIsbn(String isbn);
}