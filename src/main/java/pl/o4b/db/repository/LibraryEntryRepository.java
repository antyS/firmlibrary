package pl.o4b.db.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.o4b.db.model.library.LibraryEntry;

@Repository
public interface LibraryEntryRepository extends CrudRepository<LibraryEntry, Long> {
    LibraryEntry findFirstByBookId(Long bookId);
}



