package pl.o4b.db.model.library;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Library {

    @Id
    @GeneratedValue
    public Long id;

    @Column
    public String name;

    @ManyToMany
    public List<LibraryEntry> libraryEntries;

    @Transient
    private List<Long> libraryEntryIds;

    public List<Long> getLibraryEntryIds(){
        List<Long> idList = new ArrayList<>();
        libraryEntries.forEach(it -> idList.add(it.id));
        return idList;
    }

}
