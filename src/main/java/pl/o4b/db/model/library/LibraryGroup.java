package pl.o4b.db.model.library;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class LibraryGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column
    public String name;

    @ManyToMany
    public List<Library> libraries;

    @Transient
    public List<Long> libraryIds;

    public List<Long> getLibraryIds(){
        List<Long> idList = new ArrayList<>();
        libraries.forEach(it -> idList.add(it.id));
        return idList;
    }
}
