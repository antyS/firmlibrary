package pl.o4b.db.model.library;

import lombok.Data;
import pl.o4b.db.model.book.Book;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class LibraryEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    //TODO unused
    @Column
    public String name;

    @ManyToOne
    public Book book;

    @Column
    public Integer amount;

}
