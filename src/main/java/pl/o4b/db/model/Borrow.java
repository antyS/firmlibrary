package pl.o4b.db.model;

import lombok.Data;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.db.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.Future;
import java.util.Date;

@Data
@Entity
public class Borrow {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @OneToOne
    public LibraryEntry libraryEntry;

    @OneToOne
    public User user;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date borrowDate;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Future
    public Date expectedReturnDate;
}
