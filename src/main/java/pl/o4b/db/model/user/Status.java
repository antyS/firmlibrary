package pl.o4b.db.model.user;

public enum Status {
    BLOCKED,
    ACTIVE
}
