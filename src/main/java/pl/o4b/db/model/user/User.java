package pl.o4b.db.model.user;

import lombok.Data;
import pl.o4b.db.model.library.Library;
import pl.o4b.db.model.library.LibraryEntry;
import pl.o4b.db.model.firm.Firm;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Entity
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(nullable = false)
    @NotBlank
    public String name;

    @Column(nullable = false)
    @NotBlank
    public String surname;

    @Column(nullable = false, unique = true)
    @NotBlank
    @Email
    public String email;

    @Column(nullable = false)
    public Status status;

    @ManyToOne
    public Firm firm;

    @ManyToOne
    public Library library;

}
