package pl.o4b.db.model.book;

import lombok.Data;
import org.hibernate.validator.constraints.ISBN;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;


@Entity
@Data
public class Book {
    @ManyToMany
    List<Author> authors;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    @ManyToOne
    public Language lang;
    @Column(nullable = false)
    @NotBlank
    public String title;
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    @Past
    public Date published;
    @Column(nullable = false, unique = true)
    @NotBlank
    public String isbn;


}
