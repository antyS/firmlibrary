package pl.o4b.db.model.firm;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@Entity
public class Firm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(nullable = false)
    @NotBlank
    public String name;

    @Column(nullable = false)
    public FirmType type;

    public Integer defaultMaxBorrowCount;
    public Integer defaultMaxLentDays;
}
