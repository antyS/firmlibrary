package pl.o4b.exception;

import static org.springframework.http.HttpStatus.CONFLICT;
import static pl.o4b.exception.ExceptionMessage.DUPLICATE_EMAIL;

public class DuplicateEmailException extends BaseApplicationException {

    public DuplicateEmailException() {
        super(CONFLICT, DUPLICATE_EMAIL);
    }
}
