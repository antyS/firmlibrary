package pl.o4b.exception;

import static org.springframework.http.HttpStatus.CONFLICT;
import static pl.o4b.exception.ExceptionMessage.USER_BLOCKED;

public class UserBlockedException extends BaseApplicationException{

    public UserBlockedException() {
        super(CONFLICT, USER_BLOCKED);
    }
}
