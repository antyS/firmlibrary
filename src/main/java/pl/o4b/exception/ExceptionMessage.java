package pl.o4b.exception;

public class ExceptionMessage {
    public static final String FIRM_NOT_FOUND = "FIRM_NOT_FOUND";
    public static final String USER_NOT_FOUND = "USER_NOT_FOUND";
    public static final String LIBRARY_NOT_FOUND = "LIBRARY_NOT_FOUND";
    public static final String LIBRARY_GROUP_NOT_FOUND = "LIBRARY_GROUP_NOT_FOUND";
    public static final String LIBRARY_ENTRY_NOT_FOUND = "LIBRARY_ENTRY_NOT_FOUND";
    public static final String NO_MORE_BOOKS = "The selected book is no longer available.";
    public static final String DUPLICATE_EMAIL = "DUPLICATE_EMAIL";
    public static final String DUPLICATE_ISBN = "DUPLICATE_ISBN";
    public static final String NULL_ID = "ID_IS_NULL";
    public static final String BOOK_NOT_FOUND = "BOOK_NOT_FOUND";
    public static final String LANGUAGE_NOT_FOUND = "LANGUAGE_NOT_FOUND";
    public static final String BORROW_NOT_FOUND = "BORROW_NOT_FOUND";
    public static final String USER_BLOCKED = "User is blocked";
    static final String AUTHOR_NOT_FOUND = "AUTHOR_NOT_FOUND";
}
