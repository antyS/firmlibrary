package pl.o4b.exception;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static pl.o4b.exception.ExceptionMessage.LIBRARY_NOT_FOUND;

public class NoSuchLibraryGroupExeption extends BaseApplicationException {

    public NoSuchLibraryGroupExeption() {
        super(NOT_FOUND, LIBRARY_NOT_FOUND);
    }
}
