package pl.o4b.exception;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static pl.o4b.exception.ExceptionMessage.FIRM_NOT_FOUND;
import static pl.o4b.exception.ExceptionMessage.LIBRARY_ENTRY_NOT_FOUND;

public class NoSuchLibraryEntryExeption extends BaseApplicationException {

    public NoSuchLibraryEntryExeption() {
        super(NOT_FOUND, LIBRARY_ENTRY_NOT_FOUND);
    }
}
