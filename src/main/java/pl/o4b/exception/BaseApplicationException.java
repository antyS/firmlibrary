package pl.o4b.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public abstract class BaseApplicationException extends ResponseStatusException {

    public BaseApplicationException(final HttpStatus status, final String reason) {
        super(status, reason);
    }
}
