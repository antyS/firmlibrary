package pl.o4b.exception;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static pl.o4b.exception.ExceptionMessage.LANGUAGE_NOT_FOUND;

public class NoSuchLanguageException extends BaseApplicationException{

    public NoSuchLanguageException() {
        super(NOT_FOUND, LANGUAGE_NOT_FOUND);
    }
}
