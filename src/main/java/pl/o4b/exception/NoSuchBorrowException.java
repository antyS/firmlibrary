package pl.o4b.exception;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static pl.o4b.exception.ExceptionMessage.BORROW_NOT_FOUND;
import static pl.o4b.exception.ExceptionMessage.LIBRARY_NOT_FOUND;

public class NoSuchBorrowException extends BaseApplicationException {

    public NoSuchBorrowException() {
        super(NOT_FOUND, BORROW_NOT_FOUND);
    }
}
