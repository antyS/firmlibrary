package pl.o4b.exception;

import static org.springframework.http.HttpStatus.CONFLICT;
import static pl.o4b.exception.ExceptionMessage.DUPLICATE_EMAIL;
import static pl.o4b.exception.ExceptionMessage.NO_MORE_BOOKS;

public class NoMoreBooksException extends BaseApplicationException {

    public NoMoreBooksException() {
        super(CONFLICT, NO_MORE_BOOKS);
    }
}
