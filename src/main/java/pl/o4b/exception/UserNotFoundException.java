package pl.o4b.exception;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static pl.o4b.exception.ExceptionMessage.USER_NOT_FOUND;

public class UserNotFoundException extends BaseApplicationException {

    public UserNotFoundException() {
        super(NOT_FOUND, USER_NOT_FOUND);
    }

}
