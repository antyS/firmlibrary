package pl.o4b.exception;

import static org.springframework.http.HttpStatus.CONFLICT;
import static pl.o4b.exception.ExceptionMessage.DUPLICATE_EMAIL;
import static pl.o4b.exception.ExceptionMessage.NULL_ID;

public class NullIdException extends BaseApplicationException {

    public NullIdException() {
        super(CONFLICT, NULL_ID);
    }
}
