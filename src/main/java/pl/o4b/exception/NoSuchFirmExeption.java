package pl.o4b.exception;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static pl.o4b.exception.ExceptionMessage.FIRM_NOT_FOUND;

public class NoSuchFirmExeption extends BaseApplicationException {

    public NoSuchFirmExeption() {
        super(NOT_FOUND, FIRM_NOT_FOUND);
    }
}
