package pl.o4b.exception;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static pl.o4b.exception.ExceptionMessage.AUTHOR_NOT_FOUND;

public class NoSuchAuthorException extends BaseApplicationException {


    public NoSuchAuthorException() {
        super(NOT_FOUND, AUTHOR_NOT_FOUND);
    }
}
