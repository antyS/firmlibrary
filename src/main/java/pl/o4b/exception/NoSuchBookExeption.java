package pl.o4b.exception;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static pl.o4b.exception.ExceptionMessage.BOOK_NOT_FOUND;
import static pl.o4b.exception.ExceptionMessage.FIRM_NOT_FOUND;

public class NoSuchBookExeption extends BaseApplicationException {

    public NoSuchBookExeption() {
        super(NOT_FOUND, BOOK_NOT_FOUND);
    }
}
