package pl.o4b.exception;

import static org.springframework.http.HttpStatus.CONFLICT;
import static pl.o4b.exception.ExceptionMessage.*;

public class DuplicateIsbnException extends BaseApplicationException{

    public DuplicateIsbnException() {
        super(CONFLICT, DUPLICATE_ISBN);
    }
}
