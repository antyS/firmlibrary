insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100000, 336, 'Meerkat, red', 100000);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100001, 938, 'Tern, royal', 100001);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100002, 57, 'Teal, hottentot', 100002);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100003, 715, 'Dama wallaby', 100003);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100004, 400, 'Little heron', 100004);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100005, 475, 'Cobra (unidentified)', 100005);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100006, 476, 'Red-billed tropic bird', 100006);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100007, 609, 'Eurasian red squirrel', 100007);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100008, 44, 'Hoffman''s sloth', 100008);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100009, 345, 'Red-billed hornbill', 100009);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100010, 31, 'Common genet', 100010);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100011, 837, 'Boar, wild', 100011);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100012, 448, 'Cow, scottish highland', 100012);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100013, 401, 'Violet-crested turaco', 100013);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100014, 222, 'Partridge, coqui', 100014);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100015, 907, 'Grenadier, common', 100015);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100016, 238, 'Cheetah', 100016);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100017, 472, 'Snake, racer', 100017);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100018, 928, 'Plover, three-banded', 100018);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100019, 107, 'Greylag goose', 100019);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100020, 552, 'Egyptian cobra', 100020);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100021, 76, 'Stork, yellow-billed', 100021);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100022, 247, 'Pied avocet', 100022);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100023, 309, 'Blue-faced booby', 100023);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100024, 531, 'Brindled gnu', 100024);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100025, 526, 'Parakeet, rose-ringed', 100025);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100026, 790, 'Vine snake (unidentified)', 100026);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100027, 239, 'Sage hen', 100027);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100028, 977, 'Indian porcupine', 100028);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100029, 693, 'Hornbill, red-billed', 100029);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100030, 25, 'Bee-eater, white-fronted', 100030);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100031, 766, 'Rhesus macaque', 100031);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100032, 125, 'Phalarope, red', 100032);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100033, 694, 'Legaan, Monitor (unidentified)', 100033);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100034, 780, 'Leopard, indian', 100034);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100035, 553, 'Toucan, red-billed', 100035);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100036, 125, 'Cockatoo, roseate', 100036);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100037, 429, 'Mouflon', 100037);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100038, 441, 'Crane, brolga', 100038);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100039, 586, 'Brazilian tapir', 100039);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100040, 692, 'North American porcupine', 100040);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100041, 286, 'Goose, greylag', 100041);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100042, 587, 'Badger, honey', 100042);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100043, 546, 'Potoroo', 100043);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100044, 25, 'Common langur', 100044);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100045, 633, 'Buffalo, asian water', 100045);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100046, 624, 'Harbor seal', 100046);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100047, 475, 'Spider, wolf', 100047);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100048, 283, 'Caribou', 100048);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100049, 487, 'Indian mynah', 100049);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100050, 314, 'Serval', 100050);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100051, 558, 'Gull, kelp', 100051);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100052, 671, 'Lion, south american sea', 100052);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100053, 670, 'Vulture, white-rumped', 100053);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100054, 746, 'Common green iguana', 100054);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100055, 314, 'Urial', 100055);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100056, 845, 'Whale, killer', 100056);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100057, 232, 'Frilled lizard', 100057);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100058, 28, 'Nine-banded armadillo', 100058);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100059, 915, 'Zebra, plains', 100059);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100060, 776, 'Black-tailed deer', 100060);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100061, 838, 'Wild turkey', 100061);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100062, 705, 'Secretary bird', 100062);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100063, 11, 'Common duiker', 100063);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100064, 403, 'Mallard', 100064);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100065, 592, 'Bat, madagascar fruit', 100065);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100066, 280, 'Rhinoceros, square-lipped', 100066);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100067, 765, 'Dove, laughing', 100067);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100068, 221, 'Gerbil (unidentified)', 100068);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100069, 545, 'Bee-eater (unidentified)', 100069);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100070, 398, 'Crane, stanley', 100070);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100071, 475, 'Bonnet macaque', 100071);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100072, 543, 'Salmon, sockeye', 100072);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100073, 185, 'Grey mouse lemur', 100073);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100074, 799, 'Duck, mountain', 100074);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100075, 698, 'Caracara (unidentified)', 100075);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100076, 942, 'Galapagos sea lion', 100076);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100077, 902, 'Roadrunner, greater', 100077);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100078, 221, 'Snake (unidentified)', 100078);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100079, 612, 'Lemur, ring-tailed', 100079);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100080, 14, 'Porcupine, north american', 100080);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100081, 914, 'Raccoon, common', 100081);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100082, 354, 'Indian leopard', 100082);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100083, 885, 'Turkey vulture', 100083);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100084, 991, 'Rhinoceros, white', 100084);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100085, 456, 'Asiatic wild ass', 100085);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100086, 975, 'Common mynah', 100086);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100087, 187, 'Black-backed jackal', 100087);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100088, 692, 'Kangaroo, jungle', 100088);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100089, 674, 'Lemur, lesser mouse', 100089);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100090, 685, 'White-winged black tern', 100090);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100091, 14, 'Ibis, glossy', 100091);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100092, 592, 'North American red fox', 100092);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100093, 226, 'Blue peacock', 100093);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100094, 262, 'Red-capped cardinal', 100094);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100095, 515, 'Crane, sandhill', 100095);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100096, 411, 'Colobus, black and white', 100096);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100097, 206, 'Anteater, giant', 100097);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100098, 886, 'Badger, eurasian', 100098);
insert into LIBRARY_ENTRY (id, amount, name, book_id) values (100099, 893, 'Francolin, swainson''s', 100099);
