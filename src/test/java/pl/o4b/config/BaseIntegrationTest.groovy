package pl.o4b.config

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestPropertySource
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification

@SpringBootTest
@TestPropertySource(properties = ["spring.datasource.url=jdbc:h2:~/libraryTestDb;DB_CLOSE_DELAY=-1;AUTO_SERVER=TRUE;",
        "spring.jpa.hibernate.ddl-auto=create-drop",
        "spring.jpa.show-sql=true"])
public class BaseIntegrationTest extends Specification {
    @Autowired
    private WebApplicationContext ctx
    protected MockMvc mockMvc
    @Autowired
    ObjectMapper objectMapper


    def setup() {
        final DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.ctx)
        this.mockMvc = builder.build()
    }

}
