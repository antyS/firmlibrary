package pl.o4b.db.repository.book

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import pl.o4b.config.BaseIntegrationTest
import pl.o4b.db.model.book.Language

@SpringBootTest
class LanguageRepositoryTest extends BaseIntegrationTest {

    public static final String TEST = "test"
    @Autowired
    LanguageRepository repository

    def "save then read"() {
        given:
        def lang = new Language()
        lang.name = TEST
        when:
        def savedLang = repository.save(lang)
        def foundLang = repository.findById(savedLang.id)
        then:
        foundLang.isPresent()
        foundLang.get().name == TEST
    }
}

