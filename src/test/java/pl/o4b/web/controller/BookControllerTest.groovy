package pl.o4b.web.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.transaction.annotation.Transactional
import pl.o4b.config.BaseIntegrationTest
import pl.o4b.db.repository.book.BookRepository
import pl.o4b.service.BookService
import pl.o4b.web.controller.paths.Paths
import pl.o4b.web.dto.request.CreateBookRequest
import spock.lang.Shared
import spock.lang.Unroll

import static org.springframework.http.HttpStatus.*
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static utils.ISBNUtils.normalizeISBN

class BookControllerTest extends BaseIntegrationTest {
    @Autowired
    BookRepository bookRepository
    @Autowired
    BookService bookService
    @Shared
    def final static pastDate = new Date(1)
    @Shared
    def final static futureDate = new Date((new Date()).getTime() + (10 * 86400000))

    @Unroll
    @Transactional
//data will not be written to DB
    def "#iterationCount. crate boook with title=#title isbn=#isbn authorIds=#authorIds lang=#lang cat=#cat \
published=#published copy=#copy days=#days"() {
        given:
        def createBookRequest = CreateBookRequest.builder()
                .title(title)
                .isbn(isbn)
                .authorIds(authorIds)
                .langId(lang)
                .categoryId(cat)
                .published(published)
                .copyCount(copy)
                .maxLentDays(days)
                .build()
        def createBookRequestString = objectMapper.writeValueAsString(createBookRequest)
        when:
        def result = mockMvc.perform(post(Paths.BOOKS).content(createBookRequestString)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
        then:
        result.andDo(print())
        result.andExpect(status().is(statusCode.value()))
        if (statusCode == CREATED) {
            verifyAll {
                def bookId = Long.valueOf(result.andReturn().response.contentAsString)
                def bookOptional = bookRepository.findById(bookId)
                bookOptional.isPresent()
                def book = bookOptional.get()
                book.id == bookId
                book.authors.id == authorIds
                book.lang.id == lang
                book.category.id == cat
                book.copies.size() == copy
                book.title == title
                book.published == published
                def isbnNormalized = normalizeISBN(isbn)
                book.isbn == isbnNormalized
                book.isbn.matches("\\d{13}")
                book.maxLentDays == days
            }
        }
        where:
        title | isbn                | authorIds          | lang    | cat     | published  | copy | days || statusCode
        "foo" | "978-2-12-345680-3" | [100043L, 100051L] | 100002L | 100063L | pastDate   | 2    | 1    || CREATED
        "foo" | "9782-12-345680-3"  | [100043L, 100051L] | 100002L | 100063L | pastDate   | 2    | 1    || CREATED
        "foo" | "9782123456803"     | [100043L, 100051L] | 100002L | 100063L | pastDate   | 2    | 1    || CREATED
//              duplicate
        "foo" | "9783107348039"     | [100043L, 100051L] | 100002L | 100063L | pastDate   | 2    | 1    || CONFLICT
//            wrong isbn control sum
        "foo" | "9783107348037"     | [100043L, 100051L] | 100002L | 100063L | pastDate   | 2    | 1    || BAD_REQUEST
//                                                                                              invalid days, next invalid field diagonally to left
        "foo" | "9782123456803"     | [100043L, 100051L] | 100002L | 100063L | pastDate   | 2    | -1   || BAD_REQUEST
        "foo" | "9782123456803"     | [100043L, 100051L] | 100002L | 100063L | pastDate   | -2   | 1    || BAD_REQUEST
        "foo" | "9782123456803"     | [100043L, 100051L] | 100002L | 100063L | futureDate | 2    | 1    || BAD_REQUEST
        "foo" | "9782123456803"     | [100043L, 100051L] | 100002L | 0L      | pastDate   | 2    | 1    || NOT_FOUND
        "foo" | "9782123456803"     | [100043L, 100051L] | 0L      | 100063L | pastDate   | 2    | 1    || NOT_FOUND
        "foo" | "9782123456803"     | [0L, 100051L]      | 100002L | 100063L | pastDate   | 2    | 1    || NOT_FOUND
        "foo" | ""                  | [100043L, 100051L] | 100002L | 100063L | pastDate   | 2    | 1    || BAD_REQUEST
        ""    | "9782123456803"     | [100043L, 100051L] | 100002L | 100063L | pastDate   | 2    | 1    || BAD_REQUEST
        null  | "9782123456803"     | [100043L, 100051L] | 100002L | 100063L | pastDate   | 2    | 1    || BAD_REQUEST
        "foo" | null                | [100043L, 100051L] | 100002L | 100063L | pastDate   | 2    | 1    || BAD_REQUEST
//                                        duplicate
        "foo" | "9782123456803"     | [100043L, 100043L] | 100002L | 100063L | pastDate   | 2    | 1    || BAD_REQUEST
        "foo" | "9782123456803"     | [100043L, null]    | 100002L | 100063L | pastDate   | 2    | 1    || BAD_REQUEST
        "foo" | "9782123456803"     | []                 | 100002L | 100063L | pastDate   | 2    | 1    || BAD_REQUEST
        "foo" | "9782123456803"     | [100043L, 100051L] | null    | 100063L | pastDate   | 2    | 1    || BAD_REQUEST
        "foo" | "9782123456803"     | [100043L, 100051L] | 100002L | null    | pastDate   | 2    | 1    || BAD_REQUEST
        "foo" | "9782123456803"     | [100043L, 100051L] | 100002L | 100063L | null       | 2    | 1    || BAD_REQUEST
        "foo" | "9782123456803"     | [100043L, 100051L] | 100002L | 100063L | pastDate   | null | 1    || BAD_REQUEST

//
    }
}
