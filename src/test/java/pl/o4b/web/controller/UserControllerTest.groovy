package pl.o4b.web.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import pl.o4b.config.BaseIntegrationTest
import pl.o4b.db.repository.UserRepository
import pl.o4b.web.dto.request.CreateUserRequest
import spock.lang.Unroll

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import static pl.o4b.db.model.user.Status.ACTIVE
import static pl.o4b.db.model.user.Status.BLOCKED
import static pl.o4b.exception.ExceptionMessage.*
import static pl.o4b.web.controller.paths.Paths.USERS

class UserControllerTest extends BaseIntegrationTest {

    @Autowired
    UserRepository userRepository

    def "get user info by id"() {
        when: "request"
        final def result = mockMvc.perform(get("$USERS/100000"))
        then:
        result.andDo(print())
        result.andExpect(status().isOk())
        result.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        result.andExpect(jsonPath('$').isMap())
        result.andExpect(jsonPath('$.id').value(100000))
        result.andExpect(jsonPath('$.name').value("Eugenius"))
        result.andExpect(jsonPath('$.surname').value("Sandeman"))
        result.andExpect(jsonPath('$.email').value("esandeman0@tuttocitta.it"))
        result.andExpect(jsonPath('$.status').value("ACTIVE"))
        result.andExpect(jsonPath('$.firmId').value(100022))
        result.andExpect(jsonPath('$.maxBorrowCount').value(2))
        result.andExpect(jsonPath('$.defaultMaxLentDays').value(5))
    }

    def "non existing user id causes 404"() {
        when: "request"
        final def result = mockMvc.perform(get("$USERS/99999999999999999"))
        then:
        result.andDo(print())
        result.andExpect(status().is(404))
        result.andExpect(status().reason(USER_NOT_FOUND))
    }

    def "after create user, user is present in db"() {
        given: "create user request"
        def createUserRequest = CreateUserRequest.builder()
                .name("John")
                .surname("Kowalski")
                .email("jkowalski@metro.co.uk")
                .firmId(100000)
                .maxBorrowCount(13)
                .defaultMaxLentDays(12)
                .build()
        def createUserRequestAsString = objectMapper.writeValueAsString(createUserRequest)
        when:
        final def result = mockMvc.perform(post(USERS).content(createUserRequestAsString)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
        then:
        result.andDo(print())
        result.andExpect(status().isCreated())
        def userId = Long.valueOf(result.andReturn().response.contentAsString)
        def userOptional = userRepository.findById(userId)
        userOptional.isPresent()
        def user = userOptional.get()
        and:
        user.id == userId
        user.name == createUserRequest.name
        user.surname == createUserRequest.surname
        user.email == createUserRequest.email
        user.firm.id == createUserRequest.firmId
        user.maxBorrowCount == createUserRequest.maxBorrowCount
        user.status == ACTIVE
        user.defaultMaxLentDays == createUserRequest.defaultMaxLentDays
        cleanup:
        userRepository.deleteById(userId)
    }

    @Unroll
    def "#iterationCount. create user for name=#name surname=#surname email=#email firmId=#firmId maxLent=#maxLent borrow=borrow causes error with status code and specific cause"() {
        given: "create user request"
        def createUserRequest = CreateUserRequest.builder()
                .name(name)
                .surname(surname)
                .email(email)
                .firmId(firmId)
                .defaultMaxLentDays(maxLent)
                .maxBorrowCount(borrow)
                .build()
        def createUserRequestAsString = objectMapper.writeValueAsString(createUserRequest)
        when:
        final def result = mockMvc.perform(post(USERS).content(createUserRequestAsString)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
        then:
        result.andDo(print())
        result.andExpect(status().is(statusCode))
        if (reason) {
            result.andExpect(status().reason(reason))
        }
        where:
        name   | surname    | email                      | firmId | maxLent | borrow || statusCode || reason
        "John" | "Kowalski" | "esandeman0@tuttocitta.it" | 100000 | null    | null   || 409        || DUPLICATE_EMAIL
        "John" | "Kowalski" | "jkowalski@metro.co.uk"    | 999    | null    | null   || 404        || FIRM_NOT_FOUND
        "John" | "Kowalski" | "jkowalski@metro.co.uk"    | -1     | null    | null   || 400        || null
        "John" | ""         | "jkowalski@metro.co.uk"    | 100000 | null    | null   || 400        || null
        ""     | "Kowalski" | "jkowalski@metro.co.uk"    | 100000 | null    | null   || 400        || null
        "John" | "Kowalski" | ""                         | 100000 | null    | null   || 400        || null
        "John" | "Kowalski" | "jkowalski@metro.co.uk"    | 100000 | -1      | null   || 400        || null
        "John" | "Kowalski" | "jkowalski@metro.co.uk"    | 100000 | null    | -1     || 400        || null
        //duplicate
    }

    def "after block user is blocked in DB"() {
        given:
        Long userId = 100000
        def userBeforeBlock = userRepository.findById(userId).get()
        assert userBeforeBlock.status == ACTIVE
        when:
        def result = mockMvc.perform(put("$USERS/$userId/block"))
        then:
        result.andDo(print())
        result.andExpect(status().isNoContent())
        userRepository.findById(userId).get().status == BLOCKED
        cleanup:
        assert userBeforeBlock.status == ACTIVE
        userRepository.save(userBeforeBlock)
    }

    def "after unblock user is active in DB"() {
        given:
        Long userId = 100000
        def userBeforeUnblock = userRepository.findById(userId).get()
        userBeforeUnblock.status = BLOCKED
        userBeforeUnblock = userRepository.save(userBeforeUnblock)
        assert userBeforeUnblock.status == BLOCKED
        when:
        def result = mockMvc.perform(put("$USERS/$userId/unblock"))
        then:
        result.andDo(print())
        result.andExpect(status().isNoContent())
        userRepository.findById(userId).get().status == ACTIVE
    }

}