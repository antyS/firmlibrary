package pl.o4b.web.controller

import org.springframework.beans.factory.annotation.Autowired
import pl.o4b.config.BaseIntegrationTest
import pl.o4b.db.repository.book.AuthorRepository
import pl.o4b.web.dto.request.CreateAuthorRequest
import spock.lang.Unroll

import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import static pl.o4b.web.controller.paths.Paths.AUTHORS

class AuthorControllerTest extends BaseIntegrationTest {

    @Autowired
    AuthorRepository authorRepository

    @Unroll
    def "#iterationCount. create author with name=#name surname=#surname"() {
        given:
        def authorRequest = CreateAuthorRequest.builder()
                .name(name)
                .surname(surname)
                .build()
        def authorRequestAsString = objectMapper.writeValueAsString(authorRequest)
        when:
        def result = mockMvc.perform(post(AUTHORS).content(authorRequestAsString)
                .contentType(APPLICATION_JSON_UTF8))
        then:
        result.andDo(print())
        result.andExpect(status().is(statusCode.value()))
        if (statusCode == CREATED) {
            verifyAll {
                def authorId = Long.valueOf(result.andReturn().response.contentAsString)
                def authorOptional = authorRepository.findById(authorId)
                authorOptional.isPresent()
                def author = authorOptional.get()
                author.id == authorId
                author.name == name
                author.surname == surname
            }
        }
        where:
        name   | surname    || statusCode
        ""     | ""         || BAD_REQUEST
        "John" | ""         || BAD_REQUEST
        ""     | "Kowalski" || BAD_REQUEST
        "John" | "Kowalski" || CREATED
    }

    def "get author info"() {
        given:
        def authorId = 100000
        when:
        def result = mockMvc.perform(get("$AUTHORS/$authorId"))
        then:
        result.andDo(print())
        result.andExpect(status().isOk())
        result.andExpect(content().contentType(APPLICATION_JSON_UTF8))
        result.andExpect(jsonPath('$').isMap())
        result.andExpect(jsonPath('$.id').value(authorId))
        result.andExpect(jsonPath('$.name').value("Tove"))
        result.andExpect(jsonPath('$.surname').value("Fratson"))
    }

}

